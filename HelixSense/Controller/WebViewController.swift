//
//  WebViewController.swift
//  HelixSense
//
//  Created by Macbook Pro on 21/6/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//

import UIKit
import WebKit
import OAuthSwift
import SwiftyJSON
class WebViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let loadURL = "https://www.google.com/"
    
    
    //oAuth
    var oauthswift: OAuthSwift?
    let user = "vinoth@dcs.hs.in"
    let password = "12345"
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        doOAuth()
        // Do any additional setup after loading the view.
        self.webView.load(URLRequest(url: URL(string: self.loadURL)!))
    }
    
}

//MARK: - Web view delegate
extension WebViewController : WKNavigationDelegate, WKUIDelegate{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webView.title
        DispatchQueue.main.async {
            
            self.activityIndicator.stopAnimating()
        }
        
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.activityIndicator.startAnimating()
    }
    
}
extension WebViewController{
//    // MARK: oAuth
    func doOAuth(){
        let oauthswift = OAuth2Swift(
            consumerKey:    "clientkey",
            consumerSecret: "clientsecret",
            authorizeUrl:   "https://demo.helixsense.com/api/v2/read_group?groupby=%5B%22validation_status%22%5D&model=mro.equipment&domain=%5B%5D&fields=%5B%22display_name%22%5D",
            accessTokenUrl: "https://demo.helixsense.com/api/authentication/oauth2/token",
            responseType:   "token"
            
        )
        self.oauthswift = oauthswift
        oauthswift.authorizeURLHandler = SafariURLHandler(viewController: self, oauthSwift: oauthswift)
        let helper = oauthswift.authorize(username: "\(user)", password: "\(password)", scope: "") { result in
            switch result {
            case .success(let (credential, _, _)):
                self.callApi(oauthswift)
            case .failure(let error):
                print(error.description)
            }
        }
    }

    func callApi(_ oauthswift: OAuth2Swift) {
          let _ = oauthswift.client.get("https://demo.helixsense.com/api/v2/read_group?groupby=%5B%22validation_status%22%5D&model=mro.equipment&domain=%5B%5D&fields=%5B%22display_name%22%5D", parameters: [:]) { result in
              switch result {
              case .success(let response):
                  let jsonDict = try? response.jsonObject()
                  print(String(describing: jsonDict))
                  DispatchQueue.main.async {
                    self.popupAlert(title: "Success!!!", message: "\(jsonDict)", actionTitles: ["OK"], actions:[
                    {action1 in
                        print("")
                    }])
                }
              case .failure(let error):
                  print(error)
              }
          }
      }
}

