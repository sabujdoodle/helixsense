//
//  OccupyViewController.swift
//  HelixSense
//
//  Created by Macbook Pro on 25/7/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//

import UIKit

class OccupyViewController: UIViewController {
    @IBOutlet weak var itemTableView: UITableView!
    @IBOutlet weak var feelingLabel: UILabel!
    var locationArray : [ServiceModel] = [ServiceModel]()
    var subLocationArray : [SubLocation] = [SubLocation]()
    var db:DBHelper = DBHelper()
    var selectedChecklist:[Checklist] = []
    var checklist:[Checklist] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Occupy"
        feelingLabel.text = feelingLabel.text?.uppercased()
        populateDataOnServiceArray()
        itemTableView.separatorInset = .zero
        itemTableView.separatorColor = Colors.gray
        insertDataToSQLite()
    }
    
    @IBAction func bookButtonPressed(_ sender: UIButton) {
        popupAlert(title: "Selected Checklist", message: "\(selectedChecklist)", actionTitles: ["OK"], actions:[
        {action1 in
            print("")
        }])
    }
    private func insertDataToSQLite(){
        db.insert(shiftId: "1", checklistId: "1", checklistType: "1", question: "Do you have cough", suggestionArray: "1", answer: "1", isSubmitted: "1", headerGroup: "1", expectedAns: "1", syncStatus: 0)
        db.insert(shiftId: "2", checklistId: "2", checklistType: "1", question: "Do you have cold", suggestionArray: "1", answer: "1", isSubmitted: "1", headerGroup: "1", expectedAns: "1", syncStatus: 0)
        db.insert(shiftId: "3", checklistId: "3", checklistType: "1", question: "Do you have fever", suggestionArray: "1", answer: "1", isSubmitted: "1", headerGroup: "1", expectedAns: "1", syncStatus: 0)
        db.insert(shiftId: "4", checklistId: "4", checklistType: "1", question: "Aching throughout body", suggestionArray: "1", answer: "1", isSubmitted: "1", headerGroup: "1", expectedAns: "1", syncStatus: 0)
        db.insert(shiftId: "5", checklistId: "5", checklistType: "1", question: "Chest pain or pressure", suggestionArray: "1", answer: "1", isSubmitted: "1", headerGroup: "1", expectedAns: "1", syncStatus: 0)
        db.insert(shiftId: "6", checklistId: "5", checklistType: "1", question: "Loss of spech or movement", suggestionArray: "1", answer: "1", isSubmitted: "1", headerGroup: "1", expectedAns: "1", syncStatus: 0)
        checklist = db.read()
        itemTableView.reloadData()
      }
    private func populateSubLocationDataA(){
        let locationA = [SubLocation(name: "Block A", isSelected: false), SubLocation(name: "Block B", isSelected: false), SubLocation(name: "Block C", isSelected: false), SubLocation(name: "Block D", isSelected: false)]
        subLocationArray = locationA
        itemTableView.reloadData()
    }
    private func populateSubLocationDataB(){
        let locationB = [SubLocation(name: "Recreation Roam", isSelected: false), SubLocation(name: "Server Room", isSelected: false), SubLocation(name: "UPS Room", isSelected: false), SubLocation(name: "Work Station", isSelected: false), SubLocation(name: "Conference Room", isSelected: false)]
        subLocationArray = locationB
        itemTableView.reloadData()
    }
    private func populateSubLocationDataC(){
        let locationC = [SubLocation(name: "Block A", isSelected: false), SubLocation(name: "Block B", isSelected: false), SubLocation(name: "Block C", isSelected: false), SubLocation(name: "Block D", isSelected: false)]
        subLocationArray = locationC
        itemTableView.reloadData()
    }
    private func populateSubLocationDataD(){
        let locationDArray = [SubLocation(name: "Floor #0", isSelected: false),SubLocation(name: "Floor #1", isSelected: false),SubLocation(name: "Floor #2", isSelected: false),SubLocation(name: "Floor #3", isSelected: false)]
        subLocationArray = locationDArray
        itemTableView.reloadData()
    }
    private func populateDataOnServiceArray(){
        let workOrders = ServiceModel(name: "Building 1", image: Image(withImage: #imageLiteral(resourceName: "location")))
        locationArray.append(workOrders)
        let smartLogger = ServiceModel(name: "Building 2", image: Image(withImage: #imageLiteral(resourceName: "location")))
        locationArray.append(smartLogger)
        let tankerTransaction = ServiceModel(name: "Building 3", image: Image(withImage: #imageLiteral(resourceName: "location")))
        locationArray.append(tankerTransaction)
        let riseATicket = ServiceModel(name: "Building 4", image: Image(withImage: #imageLiteral(resourceName: "location")))
        locationArray.append(riseATicket)
    }
    
    private func goToWebView(){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    internal func resetSublocation(){
        for item in 0..<subLocationArray.count{
            subLocationArray[item].isSelected = false
        }
    }
}
//MARK: - Table View delegate and data source
extension OccupyViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checklist.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "catagoryCell", for: indexPath) as! ItemTableViewCell
        cell.selectionStyle = .none
        cell.catagoryTitle.text = checklist[indexPath.row].question
        if checklist[indexPath.row].syncStatus == 1{
            cell.selectLocationButton.tintColor = Colors.themeBlueLIGHT
            cell.selectLocationButton.setImage(UIImage(systemName: "checkmark.circle.fill"), for: .normal)
        }else {
            cell.selectLocationButton.tintColor = Colors.gray
            cell.selectLocationButton.setImage(UIImage(systemName: "circle"), for: .normal)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        checklist[indexPath.row].syncStatus = 1
        selectedChecklist.append(checklist[indexPath.row])
        itemTableView.reloadData()
    }
}
