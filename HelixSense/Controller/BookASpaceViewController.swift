//
//  BookASpaceViewController.swift
//  HelixSense
//
//  Created by Macbook Pro on 17/7/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//

import UIKit

class BookASpaceViewController: UIViewController {
    @IBOutlet weak var selectedLocationLAbel: UILabel!
    @IBOutlet weak var selectedWorkspaceLabel: UILabel!
    
    @IBOutlet weak var locationView: UIView!
    @IBOutlet var locationCollectionView: UICollectionView!{
           didSet{
               settingServiceCollectionView()
           }
       }
    
    @IBOutlet weak var itemTableView: UITableView!
    var locationArray : [ServiceModel] = [ServiceModel]()
    var subLocationArray : [SubLocation] = [SubLocation]()
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Book a space"
        [locationView].forEach({ (view) in
            view.roundCornerWithMaskedCorners(corners: [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner], radius: 10)
        })
        itemTableView.isHidden = true
        populateDataOnServiceArray()
        locationCollectionView.reloadData()
        itemTableView.separatorInset = .zero
        itemTableView.separatorColor = Colors.gray
        // Do any additional setup after loading the view.
    }
    
    @IBAction func bookButtonPressed(_ sender: UIButton) {
        goToWebView()
    }
    private func populateSubLocationDataA(){
        let locationA = [SubLocation(name: "Block A", isSelected: false), SubLocation(name: "Block B", isSelected: false), SubLocation(name: "Block C", isSelected: false), SubLocation(name: "Block D", isSelected: false)]
        subLocationArray = locationA
        itemTableView.reloadData()
    }
    private func populateSubLocationDataB(){
        let locationB = [SubLocation(name: "Recreation Roam", isSelected: false), SubLocation(name: "Server Room", isSelected: false), SubLocation(name: "UPS Room", isSelected: false), SubLocation(name: "Work Station", isSelected: false), SubLocation(name: "Conference Room", isSelected: false)]
        subLocationArray = locationB
        itemTableView.reloadData()
    }
    private func populateSubLocationDataC(){
        let locationC = [SubLocation(name: "Block A", isSelected: false), SubLocation(name: "Block B", isSelected: false), SubLocation(name: "Block C", isSelected: false), SubLocation(name: "Block D", isSelected: false)]
        subLocationArray = locationC
        itemTableView.reloadData()
    }
    private func populateSubLocationDataD(){
        let locationDArray = [SubLocation(name: "Floor #0", isSelected: false),SubLocation(name: "Floor #1", isSelected: false),SubLocation(name: "Floor #2", isSelected: false),SubLocation(name: "Floor #3", isSelected: false)]
        subLocationArray = locationDArray
        itemTableView.reloadData()
    }
    private func populateDataOnServiceArray(){
        let workOrders = ServiceModel(name: "Building 1", image: Image(withImage: #imageLiteral(resourceName: "location")))
        locationArray.append(workOrders)
        let smartLogger = ServiceModel(name: "Building 2", image: Image(withImage: #imageLiteral(resourceName: "location")))
        locationArray.append(smartLogger)
        let tankerTransaction = ServiceModel(name: "Building 3", image: Image(withImage: #imageLiteral(resourceName: "location")))
        locationArray.append(tankerTransaction)
        let riseATicket = ServiceModel(name: "Building 4", image: Image(withImage: #imageLiteral(resourceName: "location")))
        locationArray.append(riseATicket)
    }

    //MARK : - Setting Up User Collection View
     private func settingServiceCollectionView(){
         
         let cellSize = CGSize(width:100 , height:120)
         let layout = UICollectionViewFlowLayout()
         layout.scrollDirection = .horizontal //.vertical
         layout.itemSize = cellSize
         layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
         //         layout.minimumLineSpacing = 1.0
         //         layout.minimumInteritemSpacing = 1.0
         locationCollectionView.allowsMultipleSelection = false
         locationCollectionView.allowsSelection = true
         locationCollectionView.setCollectionViewLayout(layout, animated: true)
     }
    private func goToWebView(){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    internal func resetSublocation(){
        for item in 0..<subLocationArray.count{
            subLocationArray[item].isSelected = false
        }
    }
}
//MARK:- Collection view delegate and data source
extension BookASpaceViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return locationArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = locationCollectionView.dequeueReusableCell(withReuseIdentifier: "BottomCollectionViewCell", for: indexPath) as! BottomCollectionViewCell
        cell.setupCollectionView(serviceData: locationArray[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedLocationLAbel.text = locationArray[indexPath.row].name ?? ""
        UIView.animate(withDuration: 0.2) {
            self.itemTableView.isHidden = false
        }
        switch indexPath.row {
        case 0:
            populateSubLocationDataA()
        case 1:
            populateSubLocationDataB()
        case 2:
            populateSubLocationDataC()
        case 3:
            populateSubLocationDataD()
        default:
            print("")
        }
    }

    
}

extension BookASpaceViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subLocationArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "catagoryCell", for: indexPath) as! ItemTableViewCell
        cell.selectionStyle = .none
        cell.catagoryTitle.text = subLocationArray[indexPath.row].name
        if subLocationArray[indexPath.row].isSelected ?? false{
            cell.selectLocationButton.tintColor = Colors.themeBlueLIGHT
            cell.selectLocationButton.setImage(UIImage(systemName: "checkmark.circle.fill"), for: .normal)
        }else {
            cell.selectLocationButton.tintColor = UIColor.gray
            cell.selectLocationButton.setImage(UIImage(systemName: "circle"), for: .normal)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        resetSublocation()
        subLocationArray[indexPath.row].isSelected = true
        itemTableView.reloadData()
        selectedWorkspaceLabel.text = subLocationArray[indexPath.row].name ?? ""
    }
}
