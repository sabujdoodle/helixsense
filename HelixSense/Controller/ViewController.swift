//
//  ViewController.swift
//  DivyaSree
//
//  Created by Macbook Pro on 19/6/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var activityIncicator: UIActivityIndicatorView!
    @IBOutlet var workOrdersTopView: UIView!
    @IBOutlet var approvedOrdersTopView: UIView!
    @IBOutlet var secondTopView: UIView!
    @IBOutlet var waitingForBidLabel: UILabel!
    
    @IBOutlet weak var addButton: InsertButtonLayout!
    @IBOutlet weak var backButton: InsertButtonLayout!
    
    @IBOutlet var servicesCollectionView: UICollectionView!{
        didSet{
            settingServiceCollectionView()
        }
    }
    var serviceArray : [ServiceModel] = [ServiceModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.isUserInteractionEnabled = false
        activityIncicator.startAnimating()
        [addButton, backButton].forEach({ (view) in
            view.backgroundColor = Colors.themeBlue
        })
        [ workOrdersTopView, approvedOrdersTopView, secondTopView].forEach({ (view) in
            view.backgroundColor = Colors.topViewBlue
        })
        
        [workOrdersTopView,secondTopView].forEach({ (view) in
            view.roundCornerWithMaskedCorners(corners: [.layerMinXMinYCorner,.layerMaxXMinYCorner], radius: 10)
        })
        self.navigationItem.setTitle("Welcome Vinoth R", subtitle: "Chambers", time: "02:03:19", height: self.navigationController?.navigationBar.frame.height ?? 98, weidth: self.navigationController?.navigationBar.frame.width ?? 180, navigationController : navigationController!)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        populateDataOnServiceArray()
        servicesCollectionView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            self.activityIncicator.stopAnimating()
            self.view.isUserInteractionEnabled = true
        }
    }
    private func populateDataOnServiceArray(){
        let workOrders = ServiceModel(name: "Work Orders", image: Image(withImage: #imageLiteral(resourceName: "workorders")))
        serviceArray.append(workOrders)
        let smartLogger = ServiceModel(name: "Smart Logger", image: Image(withImage: #imageLiteral(resourceName: "smartlogger")))
        serviceArray.append(smartLogger)
        let tankerTransaction = ServiceModel(name: "Tanker Transaction", image: Image(withImage: #imageLiteral(resourceName: "Tankertransaction")))
        serviceArray.append(tankerTransaction)
        let riseATicket = ServiceModel(name: "Rise a Ticket", image: Image(withImage: #imageLiteral(resourceName: "smartlogger")))
        serviceArray.append(riseATicket)
        let helpdeskTicket = ServiceModel(name: "Helpdesk Ticket", image: Image(withImage: #imageLiteral(resourceName: "HelpdeskTickets")))
        serviceArray.append(helpdeskTicket)
        let handoverNotes = ServiceModel(name: "Handover Notes", image: Image(withImage: #imageLiteral(resourceName: "handovernotes")))
        serviceArray.append(handoverNotes)
        let historyCard = ServiceModel(name: "History Card", image: Image(withImage: #imageLiteral(resourceName: "workorders")))
        serviceArray.append(historyCard)
        let settings = ServiceModel(name: "Settings", image: Image(withImage: #imageLiteral(resourceName: "HelpdeskTickets")))
        serviceArray.append(settings)
        let upload = ServiceModel(name: "Upload", image: Image(withImage: #imageLiteral(resourceName: "Group")))
        serviceArray.append(upload)
        let logout = ServiceModel(name: "Logout", image: Image(withImage: #imageLiteral(resourceName: "workorders")))
        serviceArray.append(logout)
    }
    //MARK : - Setting Up User Collection View
    private func settingServiceCollectionView(){
        
        let cellSize = CGSize(width:100 , height:100)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.vertical
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        //         layout.minimumLineSpacing = 1.0
        //         layout.minimumInteritemSpacing = 1.0
        servicesCollectionView.allowsMultipleSelection = false
        servicesCollectionView.allowsSelection = true
        servicesCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    @IBAction func amountButtonPressed(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            goToWebView()
        case 2:
            goToWebView()
        case 3:
            goToWebView()
        case 4:
            goToWebView()
        case 5:
            goToWebView()
        case 6:
            goToWebView()
        case 7:
            goToWebView()
        case 8:
            goToWebView()
                    
        default:
            goToWebView()
        }
        
        
    }
    
    //as per requirments though it should not be there
    @IBAction func backButtonPressed(_ sender: UIButton) {
         goToWebView()
    }
    @IBAction func plusButtonPressed(_ sender: UIButton) {
        goToWebView()
    }
    
        private func goToWebView(){
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    
}
//MARK:- Collection view delegate and data source
extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return serviceArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = servicesCollectionView.dequeueReusableCell(withReuseIdentifier: "BottomCollectionViewCell", for: indexPath) as! BottomCollectionViewCell
        cell.setupCollectionView(serviceData: serviceArray[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        goToWebView()
    }
    
}

