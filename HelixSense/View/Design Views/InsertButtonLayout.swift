//
//  InsertButtonLayout.swift
//  DivyaSree
//
//  Created by Macbook Pro on 20/6/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//

import Foundation
import UIKit

class InsertButtonLayout: UIButton {
    
        @IBInspectable var borderWidth: CGFloat {
            set {
                layer.borderWidth = newValue
            }
            get {
                return layer.borderWidth
            }
        }
        
        @IBInspectable var cornerRadius: CGFloat {
            set {
                
                layer.cornerRadius = newValue
            }
            get {
                return layer.cornerRadius
            }
        }
        
        @IBInspectable  var borderColor: UIColor? {
            set {
                guard let uiColor = newValue else { return }
                layer.borderColor = uiColor.cgColor
            }
            get {
                guard let color = layer.borderColor else { return nil }
                return UIColor(cgColor: color)
            }
        }
    
    
    
}

