//
//  Extension.swift
//  DivyaSree
//
//  Created by Macbook Pro on 20/6/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
    func roundCornerWithMaskedCorners(corners: CACornerMask, radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = corners
        
    }
}
//MARK : add subtitle in tavigation bar
extension UINavigationItem {
    
    func setTitle(_ title: String, subtitle: String, time : String, height : CGFloat, weidth : CGFloat, navigationController : UINavigationController) {
        let appearance = UINavigationBar.appearance()
        let textColor = appearance.titleTextAttributes?[NSAttributedString.Key.foregroundColor] as? UIColor ?? .white
        
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.font = .preferredFont(forTextStyle: UIFont.TextStyle.footnote)
        titleLabel.textAlignment = .left
        titleLabel.textColor = textColor
        
        let timeLabel = UILabel()
        timeLabel.text = time
        timeLabel.textAlignment = .right
        timeLabel.font = .preferredFont(forTextStyle: UIFont.TextStyle.footnote)
        timeLabel.textColor = textColor
        let subtitleLabel = UILabel()
        subtitleLabel.text = subtitle
        subtitleLabel.font = .preferredFont(forTextStyle: UIFont.TextStyle.caption1)
        subtitleLabel.textAlignment = .left
        subtitleLabel.textColor = .white
        
        let timesubtitleLabel = UILabel()
        timesubtitleLabel.text = ""
        timesubtitleLabel.font = .preferredFont(forTextStyle: UIFont.TextStyle.caption1)
        timesubtitleLabel.textColor = .white
        
        let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.axis = .vertical
        let timeStackView = UIStackView(arrangedSubviews: [timeLabel, timesubtitleLabel])
        timeStackView.distribution = .fillEqually
        timeStackView.alignment = .fill
        timeStackView.axis = .vertical
        let rightStackView = UIStackView(arrangedSubviews: [stackView, timeStackView])
        rightStackView.distribution = .fillEqually
        rightStackView.alignment = .fill
        rightStackView.axis = .horizontal
        rightStackView.translatesAutoresizingMaskIntoConstraints = false
        let menuImageView = UIImageView()
        menuImageView.image = UIImage(systemName: "line.horizontal.3")
        menuImageView.translatesAutoresizingMaskIntoConstraints = false
        let mainStackView = UIStackView(arrangedSubviews: [menuImageView, rightStackView])
        mainStackView.distribution = .fill
        mainStackView.alignment = .center
        mainStackView.spacing = 5
        mainStackView.axis = .horizontal
//        mainStackView.addBackground(color: .red)
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        
        menuImageView.leadingAnchor.constraint(equalTo: mainStackView.leadingAnchor, constant: 0).isActive = true
        menuImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        rightStackView.leadingAnchor.constraint(equalTo: menuImageView.trailingAnchor, constant: 5).isActive = true
        rightStackView.trailingAnchor.constraint(equalTo: mainStackView.trailingAnchor, constant: 0).isActive = true
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height - 10))
        view.layer.cornerRadius = 5
        view.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.3)
        view.addSubview(mainStackView)
        mainStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 4).isActive = true
        mainStackView.widthAnchor.constraint(equalToConstant: view.frame.width - 50).isActive = true
        mainStackView.heightAnchor.constraint(equalToConstant: view.frame.height).isActive = true
        mainStackView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        mainStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        self.titleView = view
    }
}




extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}
extension Data{
    func converToString() -> String{
        return String(decoding: self, as: UTF8.self)
    }
}
//MARK : - Custom Alert with completion handler
extension UIViewController {
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
}
