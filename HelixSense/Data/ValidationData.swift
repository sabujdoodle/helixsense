//
//  ValidationData.swift
//  HelixSense
//
//  Created by Macbook Pro on 25/6/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//

import Foundation

// MARK: - ValidationData
struct ValidationData: Codable {
    let data: [Datum]?
    let error: Error?
    let length: Int?
    let status: Bool?
}

// MARK: - Datum
struct Datum: Codable {
    let domain: [[String]]?
    let validationStatus: String?
    let validationStatusCount: Int?

    enum CodingKeys: String, CodingKey {
        case domain = "__domain"
        case validationStatus = "validation_status"
        case validationStatusCount = "validation_status_count"
    }
}

// MARK: - Error
struct Error: Codable {
}
