//
//  SubLocation.swift
//  HelixSense
//
//  Created by Macbook Pro on 18/7/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//

import Foundation
// MARK: - ValidationData
struct SubLocation {
    let name: String?
    var isSelected: Bool?
}
