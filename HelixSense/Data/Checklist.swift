//
//  Checklist.swift
//  HelixSense
//
//  Created by Macbook Pro on 26/7/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//

import Foundation
// MARK: - Checklist
struct Checklist: Codable {
    let shiftId, checklistId, checklistType, question, suggestionArray, answer, isSubmitted, headerGroup, expectedAns: String
    var syncStatus: Int
    enum CodingKeys: String, CodingKey {
        case shiftId = "shift_id"
        case checklistId = "checklist_id"
        case checklistType = "checklist_type"
        case question = "question"
        case suggestionArray = "suggestion_array"
        case answer = "answer"
        case isSubmitted = "is_submitted"
        case headerGroup = "header_group"
        case expectedAns = "expected_ans"
        case syncStatus = "sync_status"
    }
}

